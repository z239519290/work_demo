
    <div style="width:100%;">
        <canvas id="canvas"></canvas>
    </div>

    <script type="text/javascript">
        var config = {
            type: 'line',
            data: {
                labels: [<?php echo $model->getLabels(); ?>],
                datasets: [{
                    label: "<?php echo $model->getRangeCaption(); ?>",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [<?php echo $model->getValues(); ?>],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:false,
                    text:'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Дата'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Сумма'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx, config);
        };

       
    </script>


