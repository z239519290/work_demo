
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <title>Chart</title>
    <script src="/js/Chart.bundle.min.js" type="text/javascript"></script>
    <script src="/js/utils.js" type="text/javascript"></script>
    <style  type="text/css">
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .forms1{
        float: left;
        padding-right:100px;
        width: 45%;
    }
    .forms2{
        padding-top: 1px;
    }
    .err{
        text-align: center;
        position: absolute;
        width: 100%;
        color: red;    
    }
    </style>
</head>

<body>
<div >
<form enctype="multipart/form-data" action="index.php" method="POST" class="forms1">
    <p>Файл: <input name="userfile" type="file" >
    <input type="submit" value="загрузить" >
    </p>
</form>

<?php $s = 'selected="selected"';?>

<form action="index.php" method="GET" class="forms2">
   <p> Группировка: <select name="group" onChange="submit();">
        <option value="all"     <?php echo $grp=='all'?$s:'';?> >Всё</option>
        <option value="hour"    <?php echo $grp=='hour'?$s:'';?> >Час</option>
        <option value="day"     <?php echo $grp=='day'?$s:'';?> >День</option>
        <option value="week"    <?php echo $grp=='week'?$s:'';?> >Неделя</option>
        <option value="mouth"   <?php echo $grp=='mouth'?$s:'';?> >Месяц</option>
    </select>
    
    </p>
</form>

</div>
<div style="float:clear;"></div>
