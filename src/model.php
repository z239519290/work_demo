<?php
class modelClass {
    private $result;
    private $first_sum;
    private $last_sum;
    private $total_count;
    function __construct($path, $uploadfile, $group = 'all') {
        require_once $path.'/phpQuery/phpQuery.php';
        $this->result = array();
        
        $doc = phpQuery::newDocument(file_get_contents($uploadfile));
        $rows = $doc->find('tr');
        $this->first_sum = NULL;
        $this->last_sum = 0;
        $this->total_count = 0;
        foreach ($rows as $row) {
            
            $first = pq($row)->find('td:first-child');

            if (empty(pq($first)->attr('colspan'))){
                $last = floatval( str_replace(" ", "", pq($row)->find('td:last-child')->html() ));            
                if ($last){
                    $this->total_count++;
                    $tm = trim(pq($row)->find('td:eq(1)')->html());
                    switch($group){
                        case 'mouth':
                            $grp = 'Y-m'; 
                            break;
                        case 'week':
                            $grp = 'Y.m W'; 
                            break;
                        case 'day':
                            $grp = 'Y.m.d'; 
                            break;
                        case 'hour':
                            $grp = 'Y.m.d H:00:00'; 
                            break;                        
                        default: 
                            $grp = 'Y.m.d H:i:s';
                    }
                    $dformat = 'Y.m.d H:i:s';
                    if (strlen($tm) == 16){
                        $dformat = 'Y.m.d H:i';
                    }
                    
                    $date = DateTime::createFromFormat($dformat, $tm);
                    $tm = $date->format($grp);
                    
                    if (empty($this->first_sum)) $this->first_sum = $last;
                    
                    $this->last_sum += $last;

                    $this->result[$tm] = array('id' => $first->html(), 'time' => $tm, 'val' => $last, 'sum' => $this->last_sum);
                }
            }
        } 
    }
    function getLabels(){
        $items = array();
        foreach($this->result as $row){
            $items[] = '"'.$row['time'].'"';
        }
        return implode(',',  $items);
    }
    function getValues(){
        $items = array();
        foreach($this->result as $row){
            $items[] = $row['sum'];
        }
        return implode(',',  $items);
    }  
    function getRangeCaption(){
        $ret = '';
        if (count($this->result) > 0){
            $first = reset($this->result);
            $last = end($this->result);
            $ret = $first['time'] . ' - ' .$last['time'];
        }
        return $ret;
    }  
    
    function getReport(){
        $ret = array(
            'first_sum' => $this->first_sum,
            'last_sum' => $this->last_sum,
            'total_count' => $this->total_count           
        );
        return $ret;
    } 
    
    function check(){
        return count($this->result) > 0;
    }
    

}



?>

