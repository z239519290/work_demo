<?php
// need php-xml

// error_reporting(E_ALL);
// ini_set("display_errors", 1);
// echo '<pre>';
$path = $_SERVER['DOCUMENT_ROOT'];


$grp = isset($_GET['group'])?$_GET['group']:'all';

require_once $path.'/src/header.php';

$uploadfile = $path . '/tmp/upload.html';


if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
     header('Location: '. $_SERVER['HTTP_ORIGIN'].$_SERVER['SCRIPT_NAME']);
} 

if (file_exists($uploadfile)){
    

    
    require_once $path.'/src/model.php';
    
    $model = new modelClass($path, $uploadfile, $grp);
    if ($model->check())
        require_once $path.'/src/chart.php';
    else echo '<p class="err">Please upload the correct file</p>';
} else {
    echo '<p  class="err">Please upload the file</p>';
}

require_once $path.'/src/footer.php';



?>

